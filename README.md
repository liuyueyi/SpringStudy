Spring学习
===

## Version

### TAG 0.0.1
***

- 学习如何在xml中配置bean，如何自动加载，如何注入实例到接口里面，这里主要可以参考 mogu.yihui.api下面的Person，Student，Manager三个的使用过程

    * Person为接口，在Manager中被依赖
    * Student为Person的实现类，并简单的扩展了一下
    * 注入方式为xml文件配置注入，相应代码为：

        <bean id="student" class="mogu.yihui.api.Student"/>
        <bean id="personService" class="mogu.yihui.api.Manager">
            <!--通过构造注入方法 可以在后面加 value 标签进行简单数据类型的注入-->
            <constructor-arg index="0" ref="student"/>
            <!--setter方法注入依赖-->
            <property name="myPerson" ref="student"/>
        </bean>

    * 因此在测试代码 mogu.hui.api.ManagerTest中的TestStudent函数可以很明显的感觉到，通过Manager返回的Person接口是可以直接当作实例使用的


- 利用@Resource，@Autowired等注解方式来实现，这里则主要看BassicInfo接口的使用方式

    * 这里主要将聚焦点放在 BasicInfo, Address, Manager的testAddress函数上
    * 首先是在BasicInfo的实现类Address上面加一个 **`@Commpoent`** 注解
    * 在Manager的 BasicInfo依赖上加一个 **`@Autowired`** 或者 **`@Resource`**注解，为了区分其中的类型或名字什么的，可以添加限定
    * 在使用的时候，直接按照一般的方式用就可以了
    * 注意，要在xml文件中添加一行 `<context:component-scan base-package="mogu.yihui"/>` 进行组件扫描
    * 另外可以对Comment和Resource进行配对，如 @Comment("address")   @Resource(name="address")这样就关联了具体的两个使用方了

    > @by author, 这种方式比上面一种各种手动配置要简洁多了, 同样缺点就是修改的时候就没有上面的方便了，需要手动修改@Resource中的指定位置

- 添加业务层逻辑代码，即各种service的加入

    * 这里面，重点是在每个serviceImpl的上方添加`@service("xxxService")
    * 测试代码中，展示了使用的方法
    * 说明： 当前在xml文件中配置了student bean的scope=prototype的时候，每次获得ApplicationContext实例时，都会实例化一个Student对象，而我们采用注解方式的Address，则并没有这方面的问题



- aop jdk动态代理

    采用jdk动态代理的方式来模拟aop的实现过程

    * Foo 接口  FooImpl接口的实现  MyInvocationHandler动态代理的关键类  MainClass 测试类
    * 使用逻辑是：
        > proxy产生一个代理
        >
        > 首先调用handler业务逻辑，调用被代理对象的具体方法，再是handler业务逻辑
        >
    * 主要是查看MyInvocationHandler类，继承自实现jdk动态代理的接口InvocationHandler，注意重写 invoke函数
    * 利用 Proxy.newProxyInstance来产生一个代理对象,其方法是:

    `Proxy.newProxyInstance(object.getClass().getClassLoader(),
                           object.getClass().getInterfaces(),
                           handler);`


### TAG 0.0.2
***

- aop 注解方式

    终于可以用注解的方式进行玩耍了！！！记录一下需要注意的问题

    * 这里主要以aop的几个文件进行测试，其中MyFooAOP是主要的面向切面的函数；FooImpl，MyTemp是被插入的类；MyAopTest是测试文件；spring-aop.xml是配置文件
    * 首先注意aop.xml的一句 `<aop:aspectj-autoproxy/>`, 同时也需要主要上面的beans xml项
    * 在maven的pom.xml中加入**aspectjweaver, aspectjrt**，这两个不加的话。。。。各种问题
    * MyFooAOP类，主要是用来写各种插入其他方法的函数，如其中的before函数；需要注意的是抬头的 @Aspect  @Component， 缺一不可
    * MyAopTest中的test2， test3方法是我们所需要的。 注意下面的`ClassPathXmlApplicationContext apc；... apc.destroy();`

        ```java
            @Test
            public void test2() {
                ClassPathXmlApplicationContext apc = new ClassPathXmlApplicationContext("spring-aop.xml");
                FooImpl foo = apc.getBean(FooImpl.class);
                System.out.println(foo.pringMessage("nihao"));
                foo.save();
                System.out.println(foo.getClass().getName());
                apc.destroy();
            }
        ```
    * FoolImpl和MyTemp中随便来个函数，用来测试，但是需要注意的是，要在前面加上 @Component用Spring自动初始化
    * ok，可以愉快的玩耍了

   ***

    * 注意1： MyFooAOP的抬头需要 @Aspect  @Component
    * 注意2： 在Maven的pom.xml中添加 aspecjweaver的依赖
    * 注意3： 测试类中，使用`ClassPathXmlAppliactionContext`代替原有的`ApplicationContext`，而且记得调用 `apc.destroy()`函数
    * 注意4： @Before里面是可以用泛指的！！！
    * 注意5： 如何在before函数中传输参数，即当我想输出当前的函数名时，怎么办（对于log类的经常用到这个）, 可以参考实例，加入参数`JoinPoint poit`




## ISSUES

***

<font color=red>1. 每次注入的对象都是同一个的问题</font>

请参考ManagerTest中的testManger函数，现在主要从Manager中获取两个Person实例，在Manager bean中添加 `scope="prototype"`是并没有什么卵用的，加了这个，通过ApplicationContext 两次获取的bean实例（即Person实例）仍然是一个

* 首先是在 manager bean上添加 `scope="prototype"`, 虽然仅加它没有什么用，但是不加就....
* 其次将注入方式改一改，如xml中的，屏蔽掉构造函数注入（这个好像用的并不多），和原来的setter方式注入，改成新的写法：`<lookup-method name="getMyPerson" bean="student"/>`
* 改了上面两个后，再试，依然没什么卵用！！！ 这时默默的在Student bean上加了一个 `scope="prototype"` , 然后神奇的事发生了...

因此从上面的几个，可以看出不仅业务逻辑方（Manager）需要添加scope限制，注入对象也需要，而且setter注入的写法也得改一改，弄成传说中的方法注入, 这节可以参考<<Spring 揭秘>>


## Append

***

附加说明：

1. mogu.reflect包下的两个方法，主要用来测试反射机制

    - RefBase: 一个被测试的类，里面包含一个私有成员，和保护方法
    - RefectTest: 测试类，利用反射机制，来设置RefBase中的私有成员，调用保护方法，打破其封闭性
    - 将焦点集中在：

    ```java

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Class clazz = loader.loadClass("mogu.reflect.RefBase");
        RefBase base = (RefBase) clazz.newInstance();

        Field name = clazz.getDeclaredField("name");
        name.setAccessible(true);
        name.set(base, "一灰");

        Method hello = clazz.getDeclaredMethod("printColor", (Class[]) null);
        hello.setAccessible(true);
        hello.invoke(base, (Objects[])null);
    ```

2. 在普通POJO上添加 **@Configuration** 注解，就可以为Spring容器定义Bean，其中每一个加入了 **@Bean** 的类方法都相当于提供了一个Bean定义的信息，关于这个可以参考

    - [User.java](src/main/java/mogu/wzb/spring/domain/User.java) : 一个基本的实体类
    - [UserBean.java](src/main/java/mogu/wzb/spring/domain/UserBean.java) ： 用类的方式为User创建bean
    - [UserServiceTest.java](src/test/java/mogu/wzb/spring/service/UserServiceTest.java) ： 测试文件, 两种测试方法，分别通过ApplicationContext和注解方式获取

3. 加上上面的，实现bean，可以有三中方式，基于XML，基于注解和基于java类

   ![三种实现bean方法](ref/三中bean方法.png)