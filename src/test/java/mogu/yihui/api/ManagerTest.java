package mogu.yihui.api;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ManagerTest extends TestCase {
    Manager manager;

    @Before
    public void setUp(){
        ApplicationContext apc = new ClassPathXmlApplicationContext("spring-config.xml");
        manager = apc.getBean(Manager.class);
    }

    @Test
    public void testStudnet(){
        // 这里我们研究一下manager中的myPerson，也是声明的接口，可是下面赋值后，我们完全将其作为一个对象来使用
        // 这里面正是利用到了spring的IoC，将Student类注入到manager的myPerson中, 下面是两个输出
        // ame: wzb sex: man age: 25 school: null
        // name: wzb sex: man age: 25 school: hust
        Person person = manager.getMyPerson();
        person.setName("wzb");
        person.setAge(25);
        person.setSex("man");
        System.out.println(person.toString());

        Student student = (Student) person;
        student.setSchool("hust");
        System.out.println(student.toString());
    }

    @Test
    public void testBasic(){
        BasicInfo basicInfo = manager.getBasicInfo();
        basicInfo.setAddress("这里呢");
        basicInfo.setPhoneNum("15978289876");
        System.out.println(basicInfo.toString());
    }

    @Test
    public void testManager(){
        // 当srping-config.xml中没有 scope = prototype的时候，下面的两个Person实例其实指向的都是一个
        // 如果添加了 scope = prototype 则结果不一样了？？？ 理论上，然而实际上行并没有什么卵用
        // 解决上面的问题依然是在修改xml文件，readme中详解
        Person person1 = manager.getMyPerson();
        Person person2 = manager.getMyPerson();

        person1.setName("hello1");
        person2.setName("hello2");
        System.out.println(person1);
        System.out.println(person2);
        if(person1 == person2){
            System.out.println("The same!");
        }
    }

}