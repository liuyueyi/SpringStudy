package mogu.yihui;

import mogu.yihui.api.Address;
import mogu.yihui.api.BasicInfo;
import mogu.yihui.service.AddressService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by yihui on 15/8/3.
 */
public class AddressServiceTest {
    AddressService addressService;

    @Before
    public void setUp(){
        ApplicationContext apc = new ClassPathXmlApplicationContext("spring-config.xml");
        addressService = apc.getBean("addressService", AddressService.class);
    }

    @Test
    public void addAddressTest(){
        BasicInfo basicInfo = new Address();
        basicInfo.setAddress("spring dot!");
        basicInfo.setPhoneNum("16978291840");

        addressService.addAddress(basicInfo);
        addressService.printAll();

        BasicInfo basicInfo1 = addressService.getAddressByPhoneNum("16978291840");
        basicInfo1.setAddress("hust d-4");
        System.out.println(basicInfo1.toString());
        System.out.println(basicInfo.toString()); // 也被修改了，引用
    }

}
