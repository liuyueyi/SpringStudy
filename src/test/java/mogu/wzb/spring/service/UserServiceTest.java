package mogu.wzb.spring.service;

import mogu.wzb.spring.dao.UserDao;
import mogu.wzb.spring.domain.User;
import mogu.wzb.spring.domain.UserBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by yihui on 15/8/11.
 */

@RunWith(SpringJUnit4ClassRunner.class) // 基于junit4的spring测试框架
@ContextConfiguration(locations = "classpath:spring-context.xml") // 启动spring容器
public class UserServiceTest {
    @Resource
    UserService service;

    @Test
    public void hasMatchUserTest() {
        String username = "wzb";
        String passwd = "wzb";
        if (service.hasMatchUser(username, passwd))
            System.out.println("yes in it");
        else
            System.out.println("sorry, no this user!");
    }

    @Test
    public void addUesrTest(){
        // 通过配置类启动容器
        ApplicationContext apc = new AnnotationConfigApplicationContext(UserBean.class);
        User user = apc.getBean("user", User.class);
        System.out.println(user);

        service.insertUser(user);
    }

    @Resource
    UserBean userBean;
    @Test
    public void addUesrTest2(){
        User user = userBean.getUser();
        System.out.println(user.toString());
    }

}
