package mogu.reflect;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Created by yihui on 15/8/12.
 *
 * 利用反射机制来规避类里面的私有成员、方法，可以直接进行调用
 */
public class RefectTest {

    @Test
    public void refectTest() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Class clazz = loader.loadClass("mogu.reflect.RefBase");
        RefBase base = (RefBase) clazz.newInstance();

        Field name = clazz.getDeclaredField("name");
        name.setAccessible(true);
        name.set(base, "一灰");


        Method hello = clazz.getDeclaredMethod("printColor", (Class[]) null);
        hello.setAccessible(true);
        hello.invoke(base, (Objects[])null);

    }

    @Test
    public void resourceTest() throws Throwable{
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource resources[] = resolver.getResources("classpath*:*.xml");
        for(Resource resource: resources){
            System.out.println(resource.getDescription());
        }
    }

}
