package mogu.wzb.spring.service;

import mogu.wzb.spring.dao.UserDao;
import mogu.wzb.spring.domain.User;

/**
 * Created by yihui on 15/8/11.
 */
public interface UserService {
    public boolean hasMatchUser(String username, String passwd);

    public void setUserDao(UserDao dao);

    public void insertUser(User user);
}
