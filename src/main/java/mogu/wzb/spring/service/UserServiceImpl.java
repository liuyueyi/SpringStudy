package mogu.wzb.spring.service;

import mogu.wzb.spring.dao.UserDao;
import mogu.wzb.spring.domain.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by yihui on 15/8/11.
 */
@Service("userService")
public class UserServiceImpl implements UserService{
    private UserDao dao;

    public boolean hasMatchUser(String username, String passwd){
        if(dao == null)
            return false;

        int matchCount = dao.getMatchCount(username, passwd);
        return matchCount > 0;
    }

    @Resource
    public void setUserDao(UserDao dao){
        this.dao = dao;
    }

    @Override
    public void insertUser(User user) {
        if(user == null)
            return;

        dao.addUser(user);
    }
}
