package mogu.wzb.spring.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

/**
 * Created by yihui on 15/8/11.
 * 通过配置类，来启动容器，在实际使用的时候，可以参考 UserServiceTest类中的addUserTest方法进行
 * 其中注意，普通的POJO只要标注了 @Configuration注解，就可以为Spring容器提供bean定义的信息，每一个标注@Bean的类方法都相当于提供了一个定义信息
 */
@Configuration
public class UserBean {
    @Bean(name = "user")
    public User buildUser() {
        User user = new User();
        user.setUsername("yihui");
        user.setPassword("yihui");
        user.setId(1);
        return user;
    }

    @Bean
    public User getUser(){
        User user = new User();
        user.setUsername("sdfak");
        user.setPassword("hkjaskjg");
        user.setId(234);
        return user;
    }
}
