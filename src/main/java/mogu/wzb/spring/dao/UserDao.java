package mogu.wzb.spring.dao;

import mogu.wzb.spring.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yihui on 15/8/11.
 */
@Repository("userDao")
public interface UserDao {
    @Select("select count(*) from user where username=#{username} and password=#{password}")
    public int getMatchCount(@Param("username") String username, @Param("password")String password);

    @Select("select * from user where username = #{username}")
    public List<User> getUserByName(@Param("username")String username);


    @Select("<script>" +
            "insert into user(id" +
            "<if test=\"#{user.username}!=null\">,username</if>" +
            "<if test=\"#{user.password}!=null\">,password</if>)" +
            "values(#{user.id}" +
            "<if test=\"#{user.username}!=null\">,#{user.username}</if>" +
            "<if test=\"#{user.password}!=null\">,#{user.password}</if>)" +
            "</script>")
    public void addUser(@Param("user") User user);
}
