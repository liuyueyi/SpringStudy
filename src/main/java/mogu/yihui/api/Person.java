package mogu.yihui.api;

/**
 * Created by yihui on 15/8/1.
 */
public interface Person {

    public void setName(String name);

    public String getName();

    public void setAge(int age);

    public int getAge();

    public void setSex(String sex);

    public String getSex();

}
