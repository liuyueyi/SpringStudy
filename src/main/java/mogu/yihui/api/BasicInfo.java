package mogu.yihui.api;

/**
 * Created by yihui on 15/8/2.
 */
public interface BasicInfo {
    public void setAddress(String address);

    public String getAddress();

    public void setPhoneNum(String phoneNum);

    public String getPhoneNum();

}
