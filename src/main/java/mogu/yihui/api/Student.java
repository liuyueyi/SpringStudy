package mogu.yihui.api;

import org.springframework.stereotype.Component;

/**
 * Created by yihui on 15/8/1.
 */
@Component
public class Student implements Person {
    private String school;
    private String name;
    private int age;
    private String sex;

    public Student(){
//        System.out.println("student class init here!");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "name: " + name + " sex: " + sex + " age: " + age + " school: " + school;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }
}
