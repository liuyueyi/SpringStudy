package mogu.yihui.api;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by yihui on 15/8/1.
 */

//@Component("address")
//@Scope(value = "prototype")
@Component
public class Address implements BasicInfo{
    private String address;
    private String phoneNum;

    public Address(){
//        System.out.println("address init here!");
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    @Override
    public String getPhoneNum() {
        return this.phoneNum;
    }

    @Override
    public String toString() {
        return "the address: " + address + " phoneNUm: " + phoneNum;
    }
}
