package mogu.yihui.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by yihui on 15/8/1.
 */
// 写了下面的东西后，在xml文件中就可以愉快的删除manager bean了而test中的获取bean方法依然那样，不用修改
@Component("manager")
public class Manager {
    // person, basicInfo 都是接口，这是面向接口的精华所在了
    private Person myPerson;

    private BasicInfo basicInfo;

    public Manager(){

    }

    public Manager(Person person){
        this.myPerson = person;
    }

    public void init(){
        System.out.println("init manager");
    }

    public void destory(){
        System.out.println("destroy manager");
    }

    public Person getMyPerson() {
        return myPerson;
    }

    public void setMyPerson(Person myPerson) {
        this.myPerson = myPerson;
    }

    public BasicInfo getBasicInfo(){
        return basicInfo;
    }

    // 这个在后面进行修改的时候，没有配置文件方便，若basicInfo希望不是注入Address,就得改了
    // 将Resource 和 @Comment进行联合使用，然后就可以愉快的自动找到注入实例了,这里就可以不写@Resource括号的东西了
    // 这里也可以使用 name = "", 值为xml中的bean的id值
    // 注解在set方法上优于卸载属性值上，因为后者会破环封装性
//    @Resource(type = Address.class) // @Resource(name="address")
    @Resource
    public void setBasicInfo(BasicInfo basicInfo){
        this.basicInfo = basicInfo;
    }
}
