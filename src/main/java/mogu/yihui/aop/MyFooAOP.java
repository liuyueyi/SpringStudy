package mogu.yihui.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by yihui on 15/8/4.
 */
@Aspect
@Component
public class MyFooAOP {
//    @Before("execution (public void mogu.yihui.aop.FooImpl.save())")
    @Before("execution(public * *())")
    public void before(JoinPoint point){
        Object[] args = point.getArgs(); // 返回目标方法的参数
        Signature signature = point.getSignature(); // 方法签名
        Object target = point.getTarget(); // 被织入增强处理的目标对象
        Object o2 = point.getTarget(); // 返回AOP框架为目标对象生成的代理对象

        System.out.println("-----before tag--------" + signature.getName());
    }

    @After("execution(public * *(*))")
    public void after(){
        System.out.println("---after function done!");
    }
}
