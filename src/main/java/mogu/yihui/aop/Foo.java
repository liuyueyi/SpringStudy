package mogu.yihui.aop;

/**
 * Created by yihui on 15/8/4.
 */
public interface Foo {
    public String pringMessage(String message);

    void save();

    void delete();

}
