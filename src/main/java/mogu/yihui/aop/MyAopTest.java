package mogu.yihui.aop;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import javax.annotation.Resource;


/**
 * Created by yihui on 15/8/4.
 */
public class MyAopTest {
    @Test
    public void test() throws Exception{
        MyInvocationHandler handler = new MyInvocationHandler();
        Foo foo = (Foo) handler.createProxyIntance(new FooImpl());
        foo.save();

        System.out.println("--------");

        foo.delete();
    }

    @Test
    public void test2() {
        ClassPathXmlApplicationContext apc = new ClassPathXmlApplicationContext("spring-aop.xml");
        Foo foo = (Foo)apc.getBean("fooImpl");
        //System.out.println("--------");
        foo.save();

        apc.destroy();
    }


    @Test
    public void test3(){
        ClassPathXmlApplicationContext apc = new ClassPathXmlApplicationContext("spring-aop.xml");
        MyTemp temp = apc.getBean(MyTemp.class);
        temp.Test();
        apc.destroy();
    }
}
