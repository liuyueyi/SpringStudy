package mogu.yihui.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by yihui on 15/8/4.
 */
public class MyInvocationHandler implements InvocationHandler {

    private Object foo;

    public Object createProxyIntance(Object target) {
        this.foo = target;
        return Proxy.newProxyInstance(this.foo.getClass().getClassLoader(),
                this.foo.getClass().getInterfaces(),
                this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object obj = null;
        try{
            beforeLog(method);
            obj = method.invoke(foo, args);
            afterLog(method);
        }catch (Exception e){
            e.printStackTrace();
        }

        return obj;
    }


    private void beforeLog(Method m){
        System.out.println("开始执行....." + m.getName());
    }

    private void afterLog(Method m){
        System.out.println("执行完毕....." + m.getName());
    }
}
