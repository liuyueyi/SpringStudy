package mogu.yihui.aop;

import org.springframework.stereotype.Component;

/**
 * Created by yihui on 15/8/4.
 */
@Component("fooImpl")
public class FooImpl implements Foo{
    public String pringMessage(String message) {
        return "This is Object FooClass! " + message;
    }

    public void save() {
        System.out.println("save test!");
    }

    public void delete() {
        System.out.println("delete test!");
    }
}
