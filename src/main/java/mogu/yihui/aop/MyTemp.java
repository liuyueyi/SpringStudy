package mogu.yihui.aop;

import org.springframework.stereotype.Component;

/**
 * Created by yihui on 15/8/9.
 */
@Component
public class MyTemp {
    public void Test(){
        System.out.println("just a temp test!");
    }
}
