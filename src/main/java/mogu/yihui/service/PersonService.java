package mogu.yihui.service;

import mogu.yihui.api.Person;

import java.util.List;

/**
 * Created by yihui on 15/8/1.
 */

public interface PersonService {
    public void addPerson(Person person);

    public Person getPersonByName(String name);

    public void printPerson();

    public List<Person> getPersonList();
}
