package mogu.yihui.service;

import mogu.yihui.api.BasicInfo;

/**
 * Created by yihui on 15/8/3.
 */
public interface AddressService {
    public void addAddress(BasicInfo basicInfo);

    public BasicInfo getAddressByPhoneNum(String num);

    public void printAll();
}
