package mogu.yihui.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.CollectionUtils;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

/**
 * Created by yihui on 15/8/7.
 */
public class MailManager {

    private static final Logger logger = LoggerFactory.getLogger(MailManager.class);

    private JavaMailSenderImpl mailSender;

    private SimpleMailMessage templateMsg;

    private MimeMessage htmlTemplateMsg;

    private String defaultSender;

    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    public void setTemplateMsg(SimpleMailMessage templateMsg) {
        this.templateMsg = templateMsg;
    }

    public void setDefaultSender(String defaultSender) {
        this.defaultSender = defaultSender;
    }

    public void sendPlainMail(Mail mail) {
        if (CollectionUtils.isEmpty(mail.getRecepients())) {
            logger.error("");
        }
        SimpleMailMessage msg = new SimpleMailMessage(this.templateMsg);
        if (null != mail.getSender()) {
            msg.setFrom(mail.getSender());
        } else {
            msg.setFrom(defaultSender);
        }
        List<String> recipients = mail.getRecepients();
        msg.setTo((String[])recipients.toArray(new String[recipients.size()]));
        msg.setText(mail.getContent());

        try {
            this.mailSender.send(msg);
        }
        catch (MailException e) {
            logger.error("send views.mail failed : {}", e);
        }
    }

    public void sendHtmlMail(Mail mail) {
        if (CollectionUtils.isEmpty(mail.getRecepients())) {
            logger.error("");
        }
        MimeMessage msg = this.mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(msg, true, "utf-8");
            msg.setFrom(new InternetAddress("yihiu@mogujie.com","hui","utf-8"));
            List<String> recipients = mail.getRecepients();
            helper.setTo((String[])recipients.toArray(new String[recipients.size()]));
            helper.setText(mail.getContent(), true);
            this.mailSender.send(msg);
        } catch (Exception e) {
            logger.error("send views.mail error : {}", e);
        }
    }
}
