package mogu.yihui.mail;

import java.util.List;

/**
 * Created by yihui on 15/8/7.
 */
public class Mail {
    /**
     * email address
     */
    private List<String> recepients;
    /**
     * email contents
     */
    private String content;
    /**
     * email title
     */
    private String title;
    /**
     * email sender
     */
    private String sender;

    public List<String> getRecepients() {
        return recepients;
    }

    public void setRecepients(List<String> recepients) {
        this.recepients = recepients;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
