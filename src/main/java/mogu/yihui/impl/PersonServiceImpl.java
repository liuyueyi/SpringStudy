package mogu.yihui.impl;

import mogu.yihui.api.Person;
import mogu.yihui.service.PersonService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yihui on 15/8/1.
 * 两种注入方式，构造函数注入和依赖注入
 */
@Service("personService")
public class PersonServiceImpl implements PersonService {
    private List<Person> personList;

    @Override
    public void addPerson(Person person) {
        if(this.personList == null){
            this.personList = new ArrayList<Person>();
        }

        if(person == null){
            System.out.println("the add personList is null ");
            return;
        }
        this.personList.add(person);
    }

    @Override
    public Person getPersonByName(String name) {
        if(name == null || name.trim().compareTo("") == 0){
            return null;
        }
        for(Person p : personList){
            if(p.getName().trim().compareTo(name.trim()) == 0)
                return p;
        }
        return null;
    }

    @Override
    public void printPerson() {
        for(Person p : this.personList){
            System.out.println(p.toString());
        }

    }

    @Override
    public List<Person> getPersonList() {
        return personList;
    }
}
