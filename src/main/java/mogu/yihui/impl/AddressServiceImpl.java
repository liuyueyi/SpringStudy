package mogu.yihui.impl;

import mogu.yihui.api.BasicInfo;
import mogu.yihui.service.AddressService;
import org.springframework.stereotype.Service;
import sun.jvm.hotspot.types.basic.BasicCIntegerField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yihui on 15/8/3.
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService {
    List<BasicInfo> list;


    @Override
    public void addAddress(BasicInfo basicInfo) {
        if(list == null )
            list = new ArrayList<BasicInfo>();
        if(basicInfo == null){
            System.out.println("the basciInfo is empty");
            return ;
        }
        list.add(basicInfo);
    }

    @Override
    public BasicInfo getAddressByPhoneNum(String num) {
        if(num == null || num.trim().compareTo("") == 0){
            System.out.println("The phone num is empty");
            return null;
        }

        for(BasicInfo info: list){
            if(info.getPhoneNum().trim().compareTo(num.trim()) == 0){
                return info;
            }
        }
        return null;
    }

    @Override
    public void printAll() {
        for(BasicInfo info: list){
            System.out.println(info.toString());
        }
    }
}
